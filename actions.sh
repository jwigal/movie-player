#! /bin/bash

# * * * * * /bin/bash /home/pi/movie-player/actions.sh
base_dir="/home/pi/movie-player"
/bin/bash $base_dir/get-leases.sh
/bin/bash $base_dir/get-netstat.sh
if [ -e "$base_dir/actions/reboot.txt" ]
then
  rm "$base_dir/actions/reboot.txt"
  echo "Rebooting..."
  /sbin/shutdown -r now
  exit 0
fi
if [ -e "$base_dir/actions/shutdown.txt" ]
then
  rm "$base_dir/actions/shutdown.txt"
  echo "Shutting Down..."
  /sbin/shutdown -h now
  exit 0
fi
exit 0
