echo "Destination          Origin                 Status   " > /home/pi/movie-player/html/netstat.txt
/bin/netstat -a | grep http | awk '{printf "%-20s %-22s %-12s \n", $4, $5, $6}' >> /home/pi/movie-player/html/netstat.txt
