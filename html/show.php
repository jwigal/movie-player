<?php 
$movie = $_GET["movie"];
if(in_array(pathinfo($movie)['extension'], array("mov", "m4v"))){
  $filetype = "mp4";
} else {
//  $filetype = preg_replace('/.*\.([a-z]{3})$/', '$1', $movie );
  $filetype = pathinfo($movie)['extension'];
//  $filetype = preg_replace('/.*\.([a-z]{3})$/', '$1', $movie );  

}

function movieNameFormatter($name){
  $name = preg_replace("/[\_]/"," ", $name);
  if (strtoupper($name) === $name) $name = ucwords(trim(strtolower($name)));
  return $name;
}

?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/app.css">
        <script src="js/vendor/modernizr-2.8.0.min.js"></script>
        <link href="css/video-js.css" rel="stylesheet">
        <script src="js/vendor/video.js"></script>
        <script>
          videojs.options.flash.swf = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']); ?>/js/vendor/video-js.swf"
        </script>
        <title>
          <?php if (isset($_GET["movie"])) echo movieNameFormatter(pathinfo($_GET["movie"])['filename'] )?>
        </title>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
      <div class="container">
          <h2>
            <?php if (isset($_GET["movie"])) echo movieNameFormatter(pathinfo($_GET["movie"])['filename'] )?>          
          </h2>
          <h3><a href="index.php">&laquo; Go Back </a></h3>

        <div class="video-container">
          <video id="example_video_1" class="video-js vjs-default-skin"
            controls preload="auto" 
            data-checksum="<?php echo substr(md5($movie),0,8);?>"
           <source src="<?php echo $movie ;?>" type='video/<?php echo $filetype ?>' />
           <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
          </video>          
        </div>

        <p id="last-played">Last played through <a href="#" class="duration">00:00:00</a>. <a href="#" class="reset-counter">Reset</p>        
      </div>

        <script src="js/vendor/jquery-1.11.1.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/vendor/jquery.cookie.1.4.1.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<!--        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>
-->
    </body>
</html>
