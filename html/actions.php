<?php 
  session_start();
  if ($_POST["reboot"]) {
    if (file_put_contents("../actions/reboot.txt", "reboot")){
      $_SESSION["results"] = "Reboot has been scheduled.";      
    } else {
      $_SESSION["results"] = "Reboot could not be scheduled.";            
    }
    header("Location: actions.php");
    exit();
  }
  
  if ($_POST["shutdown"]) {
    if (file_put_contents("../actions/shutdown.txt", "shutdown")){
      $_SESSION["results"] = "Shutdown has been scheduled.";      
    } else {
      $_SESSION["results"] = "Shutdown could not be scheduled.";            
    }
    header("Location: actions.php");
    exit();
  }
?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/app.css">
        <title>Control Panel</title>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
      <div class="container">
        
        <?php
          if (isset($_SESSION["results"])) {
            echo "<div style=\"font-weight: bold\; padding: 1em; background-color: green; color: white\">".$_SESSION["results"]."</div>";
            unset($_SESSION["results"]);
          }
        ?>
        <h1>Control Panel</h1>
        
        <form accept-charset="UTF-8" method="post">
        <input name="reboot" type="hidden" value="1">
        <input type="submit" value="Reboot" />
        </form>

        <form accept-charset="UTF-8" method="post">
        <input name="shutdown" type="hidden" value="1">
        <input type="submit"  value="Shutdown" />
        </form>
        
        <div style="margin-top: 3em">
          <a href="index.php">Back to Movies</a>
        </div>

        <h2>DHCP Leases</h2>
        <div>
<pre>
<?php  echo file_get_contents("dhcp-leases.txt"); ?>        
</pre>          
        </div>
        <h2>netstat</h2>
<pre>
<?php  echo file_get_contents("netstat.txt"); ?>        
</pre>

      </div>

    </body>
</html>

