function pad(num, size) {
    var s = "000000000" + num;
    return s.substr(s.length-size);
}

function secondsToTime(duration){
  if (duration >= (60 * 60)){
    hours = Math.floor(duration / (60 * 60));
  } else {
    hours = 0;
  }
  
  if (duration >= 60) {
    minutes = Math.floor((duration - (hours * 60 * 60)) / 60)
  } else { minutes = 0; }

  secs = duration - (hours * 60 * 60) - (minutes * 60);
  return pad(hours,2) + ":" + pad(minutes,2) + ":" +pad(secs, 2);
}

$(document).ready(function(){

  $.cookie.json = true;
  $.cookie.defaults = {
    expires: 21
  };
  
  videojs("example_video_1").ready(function(){

    var myPlayer = this;
    md5 = $("video[data-checksum]").attr("data-checksum");
    if (!$.cookie("video-player-"+md5) ){
      $.cookie("video-player-"+md5,0);
    }
     
    $(".duration").text(secondsToTime($.cookie("video-player-"+md5)));
    setInterval(function(){ 
      if (myPlayer.paused()) { 
        $("#last-played").show();
      } else {
        $("#last-played").hide();
        $.cookie("video-player-"+md5,Math.max(Math.floor(myPlayer.currentTime()), $.cookie("video-player-"+md5)));
      }
      $(".duration").text(secondsToTime($.cookie("video-player-"+md5)));
    }, 1000);
    
    $(".duration").click(function(e){
      e.preventDefault();
      if ($.cookie("video-player-"+md5) ) {
        myPlayer.currentTime($.cookie("video-player-"+md5));        
      }
    });
    $(".reset-counter").click(function(e){
      e.preventDefault();
      $.cookie("video-player-"+md5, Math.floor(myPlayer.currentTime()));
      $(".duration").text(secondsToTime(myPlayer.currentTime()));
    })
  });
});
