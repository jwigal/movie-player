<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/app.css">
        <script src="js/vendor/modernizr-2.8.0.min.js"></script>
        <link href="css/video-js.css" rel="stylesheet">
        <script src="js/vendor/video.js"></script>
        <script>
          videojs.options.flash.swf = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']); ?>/js/vendor/video-js.swf"
        </script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
      <div class="container">
        <h1>List of Movies</h1>
        <?php 

        $path = realpath('./media');
        
        // http://stackoverflow.com/questions/2930405/sort-directory-listing-using-recursivedirectoryiterator
        class ExampleSortedIterator extends SplHeap
        {
            public function __construct(Iterator $iterator)
            {
                foreach ($iterator as $item) {
                    $this->insert($item);
                }
            }
            public function compare($b,$a)
            {
                return strcmp(
                  preg_replace("/^(((the|an?) )|([^a-zA-Z0-9]+))/","",strtolower(pathinfo($a->getRealpath())['filename'])), 
                  preg_replace("/^(((the|an?) )|([^a-zA-Z0-9]+))/","",strtolower(pathinfo($b->getRealpath())['filename']))
                );
            }
        }
        
        function movieNameFormatter($name){
          $name = preg_replace("/[\_]/"," ", $name);
          if (strtoupper($name) === $name) $name = ucwords(trim(strtolower($name)));
          return $name;
        }

        $objects = new ExampleSortedIterator(
                      new RecursiveIteratorIterator(
                        new RecursiveDirectoryIterator($path, FilesystemIterator::FOLLOW_SYMLINKS)
                      )
                    );
        foreach($objects as $name){
            $path = $name->getPathname();
            $new_path = preg_replace("~.*\/(media\/.*)~", '$1',$path);
            if (!preg_match('~\/\.~',$path) && in_array(pathinfo($new_path)["extension"], array("mov","avi","mp4","mpeg", "ogg", "m4v")))
            echo "<div class='movie'><a href='show.php?movie=".urlencode($new_path)."'>".movieNameFormatter(pathinfo($new_path)['filename'])."</a></div>\n";
        }

        ?>
        
        <div style="margin-top: 3em">
          <a href="actions.php">Reboot / Shutdown</a>
        </div>
        
      </div>

        <script src="js/vendor/jquery-1.11.1.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/vendor/jquery.cookie.1.4.1.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>

